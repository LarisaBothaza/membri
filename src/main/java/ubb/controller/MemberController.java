package ubb.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import ubb.domain.Membru;
import ubb.domain.Message;
import ubb.service.MembruService;
import ubb.service.MessageService;
import ubb.utils.events.SchimbareStareEvent;
import ubb.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MemberController implements Observer<SchimbareStareEvent> {
    MembruService membruService;
    MessageService messageService;
    Membru selectedItem;
    Stage accountMemberStage;
    ObservableList<Membru> model = FXCollections.observableArrayList();
    ObservableList<Message> modelMesaje = FXCollections.observableArrayList();

    @FXML
    TableView<Membru> tableViewMembers;

    @FXML
    TableColumn<Membru, String> tableColumnNume;

    @FXML
    TableColumn<Membru, String> tableColumnRol;

    @FXML
    TableColumn<Membru, String> tableColumnStare;

    @FXML
    TableView<Message> tableViewMessages;

    @FXML
    TableColumn<Message, String> tableColumnEmitator;

    @FXML
    TableColumn<Message, String> tableColumnMesaj;

    @FXML
    TableColumn<Message, String> tableColumnOra;

    @FXML
    Button buttonMaRetrag;

    @FXML
    Button buttonRevin;

    @FXML
    Button buttonSendAll;

    @FXML
    TextField textFieldMesaj;

    @FXML
    public void initialize(){
        tableColumnNume.setCellValueFactory(new PropertyValueFactory<Membru, String>("Name"));
        tableColumnRol.setCellValueFactory(new PropertyValueFactory<Membru, String>("Rol"));
        tableColumnStare.setCellValueFactory(new PropertyValueFactory<Membru, String>("Stare"));
        tableViewMembers.setItems(model);

        tableColumnEmitator.setCellValueFactory(new PropertyValueFactory<Message, String>("NameFrom"));
        tableColumnMesaj.setCellValueFactory(new PropertyValueFactory<Message, String>("Message"));
        tableColumnOra.setCellValueFactory(new PropertyValueFactory<Message, String>("Ora"));
        tableViewMessages.setItems(modelMesaje);
    }

    public void setAttributes(MembruService membruService, MessageService messageService, Membru selectedItem,Stage accountMemberStage) {
        this.membruService = membruService;
        this.messageService = messageService;
        this.selectedItem = selectedItem;
        this.accountMemberStage = accountMemberStage;
        this.membruService.addObserver(this);
        this.messageService.addObserver(this);
        initModel();
    }

    private void initModel(){
        Iterable<Membru> activi = membruService.getMembersActive();
        List<Membru> listActive = new ArrayList<>();
        activi.forEach(membru -> {
            if(membru.getId()!= selectedItem.getId())
                listActive.add(membru);
        });
        model.setAll(listActive);

        modelMesaje.setAll((Collection<? extends Message>) messageService.getMessageToMembru(selectedItem.getId()));
    }

    @Override
    public void update(SchimbareStareEvent schimbareStareEvent) {
        initModel();
    }

    public void retragereMembru() {

        buttonMaRetrag.setVisible(false);
        buttonRevin.setVisible(true);

        membruService.updateMembru(selectedItem,"inactiv");
        initModel();
    }

    public void revinMembru(){
        buttonMaRetrag.setVisible(true);
        buttonRevin.setVisible(false);

        membruService.updateMembru(selectedItem,"activ");
        initModel();
    }

    public void sendMesaj() {
        String mesajText = textFieldMesaj.getText();
        textFieldMesaj.clear();

        Iterable<Membru> activi = membruService.getMembersActive();
        List<Membru> listActive = new ArrayList<>();
        activi.forEach(membru -> {
            if(membru.getId()!= selectedItem.getId())
                listActive.add(membru);
        });

        Message message = new Message(selectedItem, listActive ,mesajText, LocalDateTime.now());
        messageService.addMessage(message);
    }


}
