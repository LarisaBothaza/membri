package ubb.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ubb.domain.Membru;
import ubb.service.MembruService;
import ubb.service.MessageService;
import ubb.utils.events.SchimbareStareEvent;
import ubb.utils.observer.Observer;


import java.io.IOException;
import java.util.Collection;

public class IntroductionController implements Observer<SchimbareStareEvent> {
    MembruService membruService;
    MessageService messageService;
    ObservableList<Membru> model = FXCollections.observableArrayList();

    @FXML
    TableView<Membru> tableViewMembers;

    @FXML
    TableColumn<Membru, String> tableColumnNume;

    @FXML
    TableColumn<Membru, String> tableColumnRol;

    @FXML
    TableColumn<Membru, String> tableColumnStare;

    Stage introductionstage;

    @FXML
    public void initialize(){
        tableColumnNume.setCellValueFactory(new PropertyValueFactory<Membru, String>("Name"));
        tableColumnRol.setCellValueFactory(new PropertyValueFactory<Membru, String>("Rol"));
        tableColumnStare.setCellValueFactory(new PropertyValueFactory<Membru, String>("Stare"));

        tableViewMembers.setItems(model);
    }

    public void setIntroductionstage(Stage introductionstage) {
        //this.introductionstage = introductionstage;
        membruService.getAll().forEach(membru -> {
            if(membru!=null){

                if(membru.getRol().equals("sef"))
                    showAccountSef(membru);
                else
                    showAccountMembru(membru);
            }

        });
    }

    public void setMembruService(MembruService membruService) {
        this.membruService = membruService;
        this.membruService.addObserver(this);
        model.setAll((Collection<? extends Membru>) this.membruService.getAll());


    }

/*    public void selectFriendsUser(){
        UserDTO selectedUserDTO = tableViewUserDTO.getSelectionModel().getSelectedItem();
        //prietenieService.getAllFriendshipsUser(selectedUserDTO.getId()).forEach(System.out::println);
        if(selectedUserDTO != null){
            showAccountUser(selectedUserDTO);

        }

    }*/

    private void showAccountMembru(Membru selectedItem){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/memberView.fxml"));
            AnchorPane root = loader.load();

            Stage accountMemberStage = new Stage();
            accountMemberStage.setTitle(selectedItem.getName());
//            accountUserStage.initModality(Modality.APPLICATION_MODAL);
//            accountUserStage.setOnCloseRequest(event -> {
//                introductionstage.show();
//            } );

            Scene scene = new Scene(root);
            accountMemberStage.setScene(scene);
            MemberController memberController = loader.getController();

            memberController.setAttributes(membruService,messageService,selectedItem,accountMemberStage);
            //accountUserController.setIntroductionStage(introductionstage);
/*            introductionstage.hide();
            tableViewUserDTO.getSelectionModel().clearSelection();
            accountUserStage.show();*/
            accountMemberStage.show();


        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    private void showAccountSef(Membru selectedItem){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/sefView.fxml"));
            AnchorPane root = loader.load();

            Stage accountSefStage = new Stage();
            accountSefStage.setTitle(selectedItem.getName());
//            accountUserStage.initModality(Modality.APPLICATION_MODAL);
//            accountUserStage.setOnCloseRequest(event -> {
//                introductionstage.show();
//            } );

            Scene scene = new Scene(root);
            accountSefStage.setScene(scene);
            SefController sefController = loader.getController();

            sefController.setAttributes(membruService,messageService,selectedItem, accountSefStage);
            //accountUserController.setIntroductionStage(introductionstage);
/*            introductionstage.hide();
            tableViewUserDTO.getSelectionModel().clearSelection();
            accountUserStage.show();*/
            accountSefStage.show();


        }
        catch (IOException e){
            e.printStackTrace();
        }
    }


    public void selectMember(){
        Membru selectedItem = tableViewMembers.getSelectionModel().getSelectedItem();

        //prietenieService.getAllFriendshipsUser(selectedUserDTO.getId()).forEach(System.out::println);
        if(selectedItem != null){
            if(selectedItem.getRol().equals("sef"))
                showAccountSef(selectedItem);
            else
                showAccountMembru(selectedItem);
        }

    }

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    @Override
    public void update(SchimbareStareEvent schimbareStareEvent) {
        model.setAll((Collection<? extends Membru>) this.membruService.getAll());
    }
}
