package ubb.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ubb.config.ApplicationContext;
import ubb.controller.IntroductionController;
import ubb.domain.Membru;
import ubb.domain.Message;
import ubb.domain.validators.MembruValidator;
import ubb.domain.validators.MessageValidator;
import ubb.repository.Repository;
import ubb.repository.file.MembruFile;
import ubb.repository.file.MessageFile;
import ubb.service.MembruService;
import ubb.service.MessageService;

import java.io.IOException;

public class MainFX extends Application {

    private static MembruService membruService ;
    private static MessageService messageService;


    @Override
    public void start(Stage primaryStage) throws Exception {
//        Group root = new Group();
//        Scene scene = new Scene(root,500,500, Color.PINK);
//        primaryStage.setTitle("Test");
//        primaryStage.setScene(scene);
        initView(primaryStage);

           // primaryStage.setWidth(290);
           // primaryStage.setHeight(450);
            //primaryStage.setTitle("Welcome!");
           //primaryStage.show();

    }

    public static void main(String[] args) {
        //configurations
        String fileNameMembers = ApplicationContext.getPROPERTIES().getProperty("data.ubb.echipa");
        String fileNameMessage=ApplicationContext.getPROPERTIES().getProperty("data.ubb.discutiiCuSefu");

        //String fileName="data/users.csv";

        //repositories
        Repository<Long, Membru> membruFileRepository = new MembruFile(fileNameMembers, new MembruValidator());

        Repository<Long, Message> messageRepository = new MessageFile(fileNameMessage,new MessageValidator(), membruFileRepository);

        //services

        membruService = new MembruService(membruFileRepository);
        messageService = new MessageService(messageRepository);

        launch(args);
    }

    private void initView(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/introductionView.fxml"));
        AnchorPane layout = loader.load();
        primaryStage.setScene(new Scene((layout)));

        IntroductionController introductionControllerController = loader.getController();
        introductionControllerController.setMembruService(membruService);
        introductionControllerController.setMessageService(messageService);
        introductionControllerController.setIntroductionstage(primaryStage);

    }
}

