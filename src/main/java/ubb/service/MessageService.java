package ubb.service;

import ubb.domain.Membru;
import ubb.domain.Message;
import ubb.repository.Repository;
import ubb.utils.events.SchimbareStareEvent;
import ubb.utils.observer.Observable;
import ubb.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageService implements Observable<SchimbareStareEvent> {
    private Repository<Long, Message> messageRepository;
    private List<Observer<SchimbareStareEvent>> observers = new ArrayList<>();

    public MessageService(Repository<Long, Message> messageRepository) {
        this.messageRepository = messageRepository;
    }

    /**
     * add a message
     * @param messageParam
     * @return
     */
    public Message addMessage(Message messageParam){
        Message message = messageRepository.save(messageParam);
        if(message == null){
            notifyObservers(new SchimbareStareEvent());
        }
        //mesage validator sevice pt a verifica daca exista userii
        return message;
    }
    /**
     * search for a message by id
     * @param  idMessage Long
     * @return
     */
    public Message getMessage(Long idMessage){
        return messageRepository.findOne(idMessage);
    }

    public Iterable<Message> getAll(){
        return messageRepository.findAll();
    }

    public Iterable<Message> getMessageToMembru(Long id){
        Iterable<Message> listIterableAllMessage = messageRepository.findAll();

       List<Message> filterList = new ArrayList<>();

//        10;3;7,8,9;Noiembrie;2020-11-11 11:17:14
//        11;9;4,6;Salut!;2020-11-11 14:49:53

        listIterableAllMessage.forEach(message->{
            List<Membru> listUsersTo = message.getTo();

            AtomicBoolean b = new AtomicBoolean(false);
            listUsersTo.forEach(user->{
                if(user.getId().equals(id)){
                    b.set(true);
                }
            });
            if(b.get()){
                filterList.add(message);

            }
        });
        return filterList;
    }

    @Override
    public void addObserver(Observer<SchimbareStareEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<SchimbareStareEvent> e) {
        //observers.remove(e);
    }

    @Override
    public void notifyObservers(SchimbareStareEvent schimbareStareEventChangeEvent) {
        observers.forEach(obs -> obs.update(schimbareStareEventChangeEvent));
    }
}
