package ubb.service.validators;

import ubb.domain.validators.ValidationException;

public interface Validator <T> {
    void validateAdd(T entity) throws ValidationException;
    void validateDelete(T entity) throws ValidationException;
}
