package ubb.service;


import ubb.domain.Membru;
import ubb.repository.Repository;
import ubb.service.validators.Validator;
import ubb.service.validators.ValidatorMembruService;
import ubb.utils.events.ChangeEventType;
import ubb.utils.events.SchimbareStareEvent;
import ubb.utils.observer.Observable;
import ubb.utils.observer.Observer;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class MembruService implements Observable<SchimbareStareEvent> {
    private final Repository<Long, Membru> repoMembru;
    private List<Observer<SchimbareStareEvent>> observers = new ArrayList<>();

    //private ValidatorMembruService<Membru> validatorMembruService;
    private final Validator<Membru> validatorMembruService = new ValidatorMembruService<>();

    public MembruService(Repository<Long, Membru> repoMembru) {
        this.repoMembru = repoMembru;

    }

    /**
     *saves the user received parameter
     * @param MembruParam
     * @return the user created
     */
    public Membru addMembru(Membru MembruParam) {
        Membru Membru = repoMembru.save(MembruParam);
        validatorMembruService.validateAdd(Membru);
        return Membru;
    }

    ///TO DO: add other methods

    /**
     *delete the user identified by the received id, and also delete that user's friendships
     * @param id long - id's to be deleted
     * @return deleted user
     * @throws ConcurrentModificationException
     */
    public Membru deleteMembru(Long id) throws ConcurrentModificationException {
        Membru membru = repoMembru.delete(id);
        //validatorMembruService.validateDelete(membru);
        if(membru != null){
            notifyObservers(new SchimbareStareEvent(ChangeEventType.DELETE,membru));
        }

        return membru;
    }

    /**
     *
     * @return an iterable list of all saved users
     */
    public Iterable<Membru> getAll(){
        return repoMembru.findAll();
    }
    public Membru findOne(Long id){
        return repoMembru.findOne(id);
    }

    public Iterable<Membru> getAllMembers(){

        Iterable<Membru> list =  repoMembru.findAll();
        List<Membru> memberList = new ArrayList<>();
        list.forEach(membru -> {
            if(membru.getRol().equals("membru"))
                memberList.add(membru);
        });
        return memberList;
    }

    public Iterable<Membru> getMembersActive(){

        Iterable<Membru> list =  repoMembru.findAll();
        List<Membru> memberList = new ArrayList<>();
        list.forEach(membru -> {
            if(membru.getStare().equals("activ"))
                memberList.add(membru);
        });
        return memberList;
    }

    public Membru findSef(){

        Iterable<Membru> list =  repoMembru.findAll();

        for(Membru membru:list)
            if(membru.getRol().equals("sef"))
                return membru;


            return null;
    }

    public void updateMembru(Membru membru, String status){
        Membru fr = deleteMembru(membru.getId());
        fr.setStare(status);

        fr = addMembru(fr);

        //membru.setStare(status);

        notifyObservers(new SchimbareStareEvent(ChangeEventType.UPDATE,membru));

    }

    @Override
    public void addObserver(Observer<SchimbareStareEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<SchimbareStareEvent> e) {
        //observers.remove(e);
    }

    @Override
    public void notifyObservers(SchimbareStareEvent schimbareStareEventChangeEvent) {
        observers.forEach(obs -> obs.update(schimbareStareEventChangeEvent));
    }
}
