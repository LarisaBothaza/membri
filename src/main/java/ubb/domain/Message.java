package ubb.domain;

import ubb.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long> {
    private static long idMaxMesaj=0;
    private Membru from;
    private List<Membru> to;
    private String message;
    private LocalDateTime data;

    public Message(Membru from, List<Membru> to, String message, LocalDateTime data) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.data = data;
        idMaxMesaj++;
        setId(idMaxMesaj);
    }

    public Membru getFrom() {
        return from;
    }

    public void setFrom(Membru from) {
        this.from = from;
    }

    public List<Membru> getTo() {
        return to;
    }

    public void setTo(List<Membru> to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getData() {
        return data;
    }

    public int getOra() {
        return data.getHour();
    }

    public String getDateString(){
        return data.format(Constants.DATE_TIME_FORMATTER);
    }

    public String getNameFrom(){
        return from.getName();
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", to=" + to +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return Objects.equals(from, message1.from) &&
                Objects.equals(to, message1.to) &&
                Objects.equals(message, message1.message) &&
                Objects.equals(data, message1.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to, message, data);
    }
}
