package ubb.domain.validators;

import ubb.domain.Membru;

public class MembruValidator implements Validator<Membru> {
    @Override
    public void validate(Membru entity) throws ValidationException {
        //TODO: implement method validate
        String err = "";
        if(entity.getName().matches(".*\\d.*") || entity.getName().length()<3 || entity.getName().equals("")){
            err+="Name invalid!\n";
        }

        if(err.length() > 0)
            throw new ValidationException(err);
    }
}
