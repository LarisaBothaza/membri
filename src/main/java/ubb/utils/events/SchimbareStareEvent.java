package ubb.utils.events;

import ubb.domain.Membru;

public class SchimbareStareEvent implements Event{
    private ChangeEventType eventType;
    private Membru oldStare, newStare;

    public SchimbareStareEvent(ChangeEventType eventType, Membru oldStare) {
        this.eventType = eventType;
        this.newStare = oldStare;
    }

    public SchimbareStareEvent(ChangeEventType eventType, Membru oldStare, Membru newStare) {
        this.eventType = eventType;
        this.oldStare = oldStare;
        this.newStare = newStare;
    }

    public SchimbareStareEvent() {
    }

    public ChangeEventType getEventType() {
        return eventType;
    }

    public Membru getOldStare() {
        return oldStare;
    }

    public Membru getNewStare() {
        return newStare;
    }
}
