package ubb.utils.observer;

import ubb.utils.events.Event;

public interface Observer<E extends Event> {
    void update(E e);
}