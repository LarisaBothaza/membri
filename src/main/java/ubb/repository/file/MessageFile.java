package ubb.repository.file;

import ubb.domain.Membru;
import ubb.domain.Message;
import ubb.domain.validators.Validator;
import ubb.repository.Repository;
import ubb.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessageFile extends AbstractFileRepository<Long, Message> {

    public MessageFile(String fileName, Validator<Message> validator, Repository<Long, Membru> membruRepository) {

        super(fileName,validator,membruRepository);
    }



    /**
     * extract the attributes of a  message from a file line
     * @param attributes
     * @return message
     */
    @Override
    public Message extractEntity(List<String> attributes) {
//        Message message = new Message(new Membru("Dummy","Dummy"), new ArrayList<>(),"Dummy", LocalDateTime.now());
//        message.setId(Long.parseLong(attributes.get(0)));
//        return message;

        //14;3;4,5,6;FInal;2020-11-11 18:52:03

        Long idUserEmitator = Long.parseLong(attributes.get(1));

        String list = attributes.get(2);
        String[] parts = list.split(",");
        List<Membru> idsUsersReceptors = new ArrayList<>();
        for(String p : parts){
            Long id = Long.parseLong(p);
            Membru user = membruRepository.findOne(id);
            idsUsersReceptors.add(user);
        }

        String textMesaj = attributes.get(3);
        LocalDateTime data = LocalDateTime.parse(attributes.get(4),Constants.DATE_TIME_FORMATTER);

        Message message =  new Message(membruRepository.findOne(idUserEmitator),idsUsersReceptors,textMesaj, data);
        message.setId(Long.parseLong(attributes.get(0)));
        return message;
    }
    /**
     * create a line for file from the attributes of a  message
     * @param entity
     * @return string
     */
    @Override
    protected String createEntityAsString(Message entity) {
        String listTo = "";
        List<Membru> list = entity.getTo();
        for(Membru user : list){
            listTo += user.getId()+",";
        }
        if(listTo.length() >= 1)
            listTo = listTo.substring(0,listTo.length()-1);

        String messageAttributes = "";
        messageAttributes += entity.getId() + ";"
                            + entity.getFrom().getId() + ";"
                            + listTo + ";"
                            + entity.getMessage() + ";"
                            + entity.getData().format(Constants.DATE_TIME_FORMATTER);
        return messageAttributes;
    }
}
